<?php

namespace App\Http\Controllers;

use App\Agenda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AgendaController extends Controller
{


    /**index = Exibe uma lista de registros na tabela
     * create = Exibe um form para cadastrar dados
     * store = recebe os dados do form(create) e envia para o model
     *         gravar na tabela (operação de inserção)
     * edit= exibe um form para alterar dados de um determinado registro lido
     * update = recebe os dados do form (edit) e envia para o model 
     *          atualizar na tabela (operação de update)
     * show = exibe um detalhe os dados de um determinado registro
     * destroy = deleta um derminado registro 
     */
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contatos = Agenda ::all();
        return view ('agenda.index',compact('contatos'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('agenda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$dt_nasc= $this->formata_data($request->dt_nasc);

        DB::table('agendas')->insert([
            'nome'=>$request->nome,
            'fone_Res'=>$request->fone_Res,
            'dt_nasc'=>$request->dt_nasc,
            'email'=>$request->email,
            'facebook'=>$request->facebook,
            'twitter'=>$request->twitter,
            'fone_cel'=>$request->fone_cel,
            'instagram'=>$request->instagram

        ]);


       // return view('agenda.index');
        
        return redirect ()->route('agenda.index');
    }

   //* public function formata_data($data){
        //dd/mm/aaaa
        //$d =substr($data,6,4) . "-".substr($data,3,2) . "-" . substr($data,0, 2);
        //return $d;
   // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // 
        $agenda = Agenda::find($id);

        return view('agenda.show',compact('agenda'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $agenda = Agenda::find($id);

        return view('agenda.edit',compact('agenda'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
       //dd($request,$id);
        
        DB::table('agendas')
        ->where("id",$id)
        ->update([
            'nome' => $request->nome,
            'fone_res' => $request->fone_res,
            'dt_nasc' => $request->dt_nasc,
            'email' => $request->email,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'fone_cel' => $request->fone_cel,
            'instagram' => $request->instagram
        ]);


        return redirect()->route('agenda.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agenda $agenda)
    {
        //
    }
}
