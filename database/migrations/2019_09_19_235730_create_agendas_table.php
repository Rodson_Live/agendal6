<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proprietario', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('nome',70);
            $table->string('rg',70);
            $table->string('cpf',70);
            $table->string('fone_res',15);
            $table->string('fone_cel',15);
            $table->date('dt_nasc');
            $table->string('email',50)
            $table->string('facebook',20);
            $table->string('twitter',20);
            $table->string('comprovante titulo',30);
        

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendas');
    }
}
