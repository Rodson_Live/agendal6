@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Exibição dos dados de Contato</h1>
@stop

@section('content')
<div class= "panel panel-default">
    <div class="panel-heading">
    

    </div>

    <div class="panel-body">
        <table class="table table-bordered table-hover
        table-striped">
         </tbody>
            <tr>
                <td class="col-md-2">ID</td>
                <td>{{$agenda->id}}</td>
            </tr>

            <tr>
                <td class="col-md-2">Nome do contato</td>
                <td>{{$agenda->nome}}</td>
            </tr>

            <tr>
                <td class="col-md-2">Fone residencial</td>
                <td>{{$agenda->fone_res}}</td>
            </tr>

            <tr>
                <td class="col-md-2">Fone celular</td>
                <td>{{$agenda->fone_cel}}</td>
            </tr>

            <tr>
                <td class="col-md-2">Data de nascimento</td>
                <td class="col-md-10">{{date('d/m/Y',strtotime($agenda->dt_nasc))}}</td>
            </tr>

            <tr>
                <td class="col-md-2">E-mail para contato</td>
                <td>{{$agenda->email}}</td>
            </tr>

            <tr>
                <td class="col-md-2">Facebook</td>
                <td>{{$agenda->facebook}}</td>
            </tr>

            <tr>
                <td class="col-md-2">twitter</td>
                <td>{{$agenda->twitter}}</td>
            </tr>

            <tr>
                <td class="col-md-2">instagram</td>
                <td>{{$agenda->instagram}}</td>
            </tr>
         </tbody>
        </table>
    </div>

    <div class="panel-footer">
         <a href="{{ route('agenda.index') }}" class="btn btn-default">
              <i class="fas fa-reply"></i>Voltar

        </a>
    
    </div>

</div>
@stop

@section('css')

@stop

@section('js')

@stop